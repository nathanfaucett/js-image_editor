import { Router } from "@nathanfaucett/web_app";
import { Editor, Home, NotFound, SignIn } from "./components/pages";
import app from "./app";
import autoSignIn from "./middleware/autoSignIn";
import isSignedIn from "./middleware/isSignedIn";
import oauthSignIn from "./middleware/oauthSignIn";

const router = app.plugin(Router);

router.use("/oauth", oauthSignIn);

router.route("sign_in", "/sign_in", SignIn);

router.use(autoSignIn);
router.use(isSignedIn);

router.route("index", "/", Home);
router.route("editor", "/editor/:id", Editor);

router.use(app.plugin(Router).notFound(NotFound));
