import { App } from "@nathanfaucett/web_app";
import config from "./config.json";

class ExampleApp extends App {
	constructor(config) {
		super(config, {
			restoreAppStateOnLoad: false,
			appStateKey: "ImageEditor-State",
			Locales: {
				header: "X-ImageEditor-User-Locale"
			}
		});

		this.request.defaults.headers.Accept = "application/json";
		this.request.defaults.headers["Content-Type"] = "application/json";
		this.request.defaults.withCredentials = true;
	}
}

export default new ExampleApp(config);
