import app from "../app";

const USER_TOKEN = "X-ImageEditor-User-Token";

const user = app.createStore("user", {
    email: "",
    tokenId: null
});

user.isSignedIn = () => {
    return !!app.request.defaults.headers.Authorization;
};

user.autoSignIn = callback => {
    const tokenId = app.cookies.get(USER_TOKEN);

    if (tokenId) {
        user.signInWithTokenId(tokenId, callback);
    } else {
        callback(new Error("Session id not available"));
    }
};

user.signInWithTokenId = (tokenId, callback) => {
    if (!user.isSignedIn()) {
        app.request
            .get(app.config.apiUrl + "/user", {
                headers: { Authorization: "Bearer " + tokenId }
            })
            .then(response => {
                app.cookies.set(USER_TOKEN, tokenId);
                app.request.defaults.headers.Authorization =
                    "Bearer " + tokenId;

                user.updateState(state =>
                    state
                        .set("tokenId", tokenId)
                        .set("email", response.data.data.email)
                );
                callback();
            })
            .catch(callback);
    } else {
        callback();
    }
};

user.signOut = callback => {
    if (user.isSignedIn()) {
        app.request
            .delete(app.config.apiUrl + "/user/sign_out")
            .then(() => {
                app.cookies.remove(USER_TOKEN);
                delete app.request.defaults.headers.Authorization;

                user.updateState(state =>
                    state.set("tokenId", null).set("email", "")
                );
                callback();
            })
            .catch(callback);
    } else {
        callback();
    }
};

export default user;
