import { Map, List, fromJS } from "immutable";
import app from "../app";

const clippings = app.createStore("clippings", {});

const COLORS = ["black", "blue", "brown", "green", "red", "orange", "yellow"];
const randomColor = () => COLORS[(Math.random() * COLORS.length) | 0];

const pointsWithIn = (x0, y0, x1, y1, r) => {
    const dx = x1 - x0,
        dy = y1 - y0;

    return dx * dx + dy * dy < r * r;
};

const createClip = point =>
    Map({
        color: randomColor(),
        closed: false,
        points: List([List(point)])
    });
const createClipping = point =>
    Map({
        current: 0,
        highlighted: -1,
        list: List([createClip(point)])
    });
const createClipFromPoints = points =>
    Map({
        color: randomColor(),
        closed: true,
        points: fromJS(points)
    });
const createClippingPoints = list =>
    Map({
        current: list.length,
        highlighted: -1,
        list: List(list.map(createClipFromPoints)).push(createClip([0, 0]))
    });

clippings.updateInitialPoint = (imageId, point) => {
    clippings.updateState(state => {
        return state.update(imageId, clipping => {
            if (clipping) {
                return clipping.update("list", list =>
                    list.update(clipping.get("current"), clip =>
                        clip.update("points", points =>
                            points.set(points.size - 1, List(point))
                        )
                    )
                );
            } else {
                return createClipping(point);
            }
        });
    });
};

clippings.updatePoint = (imageId, point, minDist) => {
    clippings.updateState(state =>
        state.update(imageId, clipping => {
            let current = clipping.get("current"),
                list = clipping.get("list"),
                currentPoints = list.get(current),
                points = currentPoints.get("points"),
                first = points.get(0);

            if (
                points.size > 3 &&
                pointsWithIn(
                    point[0],
                    point[1],
                    first.get(0),
                    first.get(1),
                    minDist
                )
            ) {
                return clipping.update("list", list =>
                    list.update(current, clip =>
                        clip.update("points", points =>
                            points.set(points.size - 1, first)
                        )
                    )
                );
            } else {
                return clipping.update("list", list =>
                    list.update(current, clip =>
                        clip.update("points", points =>
                            points.set(points.size - 1, List(point))
                        )
                    )
                );
            }
        })
    );
};

clippings.addPoint = (imageId, point, minDist) => {
    let closed = false;

    clippings.updateState(state =>
        state.update(imageId, clipping => {
            let current = clipping.get("current"),
                list = clipping.get("list"),
                currentPoints = list.get(current),
                points = currentPoints.get("points"),
                first = points.get(0);

            if (
                points.size > 3 &&
                pointsWithIn(
                    point[0],
                    point[1],
                    first.get(0),
                    first.get(1),
                    minDist
                )
            ) {
                closed = true;
                return clipping
                    .update("current", current => current + 1)
                    .update("list", list =>
                        list
                            .update(current, clip =>
                                clip
                                    .set("closed", true)
                                    .update("points", points => points.pop())
                            )
                            .push(createClip(point))
                    );
            } else {
                return clipping.update("list", list =>
                    list.update(current, clip =>
                        clip.update("points", points =>
                            points.push(List(point))
                        )
                    )
                );
            }
        })
    );

    return closed;
};

clippings.remove = (imageId, index) => {
    clippings.updateState(state =>
        state.update(imageId, clipping =>
            clipping
                .update("current", current => current - 1)
                .update(
                    "highlighted",
                    currentIndex => (currentIndex === index ? -1 : currentIndex)
                )
                .update("list", list => list.remove(index))
        )
    );
};

clippings.highlight = (imageId, index) => {
    clippings.updateState(state =>
        state.update(imageId, clipping =>
            clipping.update(
                "highlighted",
                currentIndex => (currentIndex === index ? -1 : index)
            )
        )
    );
};

clippings.fromPoints = (imageId, points) => {
    clippings.updateState(state =>
        state.set(imageId, createClippingPoints(points))
    );
};

clippings.points = imageId =>
    clippings
        .state()
        .get(imageId)
        .get("list")
        .map(clipping => clipping.get("points"))
        .pop();

export default clippings;
