import { fromJS, Map } from "immutable";
import app from "../app";

const images = app.createStore("images");

const createImage = image =>
    fromJS({
        id: image.id + "",
        url: image.url,
        points: JSON.parse(image.points)
    });

images.all = callback => {
    app.request
        .get(app.config.apiUrl + "/images")
        .then(response => {
            let nextState = Map();

            const imagesData = response.data.data,
                imageArray = imagesData.map(imageData => {
                    const image = createImage(imageData);
                    nextState = nextState.set(image.get("id"), image);
                    return image;
                });

            images.replaceState(nextState);

            callback(undefined, imageArray);
        })
        .catch(callback);
};

images.get = (id, callback) => {
    const image = images.state().get(id);

    if (!image) {
        app.request
            .get(app.config.apiUrl + "/images/" + id)
            .then(response => {
                const imageData = response.data.data,
                    image = createImage(imageData);

                images.updateState(state => state.set(image.get("id"), image));

                callback(undefined, image);
            })
            .catch(callback);
    } else {
        callback(undefined, image);
    }
};

images.upload = (formData, callback) => {
    app.request
        .post(app.config.apiUrl + "/images", formData, {
            headers: {
                Accept: "multipart/form-data",
                "Content-Type": "multipart/form-data"
            }
        })
        .then(response => {
            const imageData = response.data.data,
                image = createImage(imageData);

            images.updateState(state => state.set(image.get("id"), image));

            callback(undefined, image);
        })
        .catch(callback);
};

images.update = (id, points, callback) => {
    app.request
        .put(app.config.apiUrl + "/images/" + id, {
            points: JSON.stringify(points)
        })
        .then(response => {
            const imageData = response.data.data,
                image = createImage(imagesData);

            images.updateState(state => state.set(image.get("id"), image));

            callback(undefined, image);
        })
        .catch(callback);
};

export default images;
