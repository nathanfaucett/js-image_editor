import React from "react";
import Layout from "../layout/Layout";
import Gallery from "../Gallery";

class Home extends React.Component {
	render() {
		return (
			<Layout className="Home">
				<Gallery />
			</Layout>
		);
	}
}

export default Home;
