import React from "react";
import PropTypes from "prop-types";
import { Router } from "@nathanfaucett/web_app";
import Layout from "../layout/Layout";
import ImageEditor from "../ImageEditor";

class Editor extends React.Component {
	render() {
		return (
			<Layout className="Editor">
				<ImageEditor id={this.context.ctx.params.id} />
			</Layout>
		);
	}
}

Editor.contextTypes = {
	ctx: PropTypes.object
};

export default Editor;
