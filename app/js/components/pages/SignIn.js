import React from "react";
import { withStyles } from "material-ui/styles";
import Button from "material-ui/Button";
import Typography from "material-ui/Typography";
import { FormattedMessage } from "react-intl";
import LayoutNoHeader from "../layout/LayoutNoHeader";
import app from "../../app";

const styles = () => ({
    container: {
        margin: "0px auto",
        width: "320px"
    }
});

class SignIn extends React.Component {
    render() {
        return (
            <LayoutNoHeader>
                <div className={this.props.classes.container}>
                    <Typography variant="title">
                        <FormattedMessage id="sign_in.sign_in" />
                    </Typography>
                    <Button href={app.config.apiUrl + "/auth/github"}>
                        Github
                    </Button>
                    <Button href={app.config.apiUrl + "/auth/google"}>
                        Google
                    </Button>
                </div>
            </LayoutNoHeader>
        );
    }
}

export default withStyles(styles)(SignIn);
