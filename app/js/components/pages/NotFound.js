import React from "react";
import { withStyles } from "material-ui/styles";
import Typography from "material-ui/Typography";
import { FormattedMessage } from "react-intl";
import Layout from "../layout/Layout";

const styles = () => ({
    container: {
        margin: "0px auto",
        width: "320px"
    }
});

class NotFound extends React.Component {
    render() {
        return (
            <Layout className="NotFound">
                <div className={this.props.classes.container}>
                    <Typography variant="title">
                        <FormattedMessage id="error.not_found.code" />
                    </Typography>
                    &nbsp;
                    <Typography variant="body2">
                        <FormattedMessage id="error.not_found.message" />
                    </Typography>
                </div>
            </Layout>
        );
    }
}

export default withStyles(styles)(NotFound);
