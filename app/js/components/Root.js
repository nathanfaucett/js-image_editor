import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import { CircularProgress } from "material-ui/Progress";
import { IntlProvider } from "react-intl";
import connect from "@nathanfaucett/state-react";
import { Router, Locales } from "@nathanfaucett/web_app";
import app from "../app";

const routerPlugin = app.plugin(Router),
    routerStore = routerPlugin.store(),
    localeStore = app.plugin(Locales).store();

const styles = () => ({
    progress: {
        display: "inline-block",
        padding: "128px"
    },
    progressContainer: {
        position: "relative",
        textAlign: "center"
    }
});

class Root extends React.Component {
    getChildContext() {
        return {
            locale: this.props.locales.locale,
            ctx: this.props.router.ctx
        };
    }
    render() {
        let state = this.props.router.state,
            Component = routerPlugin.data(state),
            locale = this.props.locales.locale,
            messages = this.props.locales.locales[locale];

        if (Component && messages) {
            return (
                <div className="Root">
                    <IntlProvider locale={locale} messages={messages}>
                        <Component />
                    </IntlProvider>
                </div>
            );
        } else {
            return (
                <div className="Root">
                    <div className={this.props.classes.progressContainer}>
                        <CircularProgress
                            className={this.props.classes.progress}
                        />
                    </div>
                </div>
            );
        }
    }
}

Root.childContextTypes = {
    locale: PropTypes.string,
    ctx: PropTypes.object
};

export default connect(withStyles(styles)(Root), [routerStore, localeStore]);
