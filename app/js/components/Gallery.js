import React from "react";
import { findDOMNode } from "react-dom";
import connect from "@nathanfaucett/state-react";
import { withStyles } from "material-ui/styles";
import images from "../stores/images";
import GalleryItem from "./GalleryItem";
import app from "../app";

const styles = () => ({
    container: {}
});

class Gallery extends React.Component {
    constructor(props) {
        super(props);

        this.onFileUpload = e => {
            const data = new FormData();
            data.append("file", e.target.files[0]);
            images.upload(data, () => {});
        };
    }
    componentDidMount() {
        images.all(() => {});
    }
    render() {
        return (
            <div className={this.props.classes.container}>
                <input type="file" onChange={this.onFileUpload} />
                <div>
                    {Object.values(this.props.images).map(image => (
                        <GalleryItem key={image.id} image={image} />
                    ))}
                </div>
            </div>
        );
    }
}

export default connect(withStyles(styles)(Gallery), [images]);
