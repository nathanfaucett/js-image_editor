import React from "react";
import PropTypes from "prop-types";
import Button from "material-ui/Button";
import { FormattedMessage } from "react-intl";
import connect from "@nathanfaucett/state-react";
import clippings from "../../stores/clippings";
import images from "../../stores/images";

class Clipping extends React.Component {
    constructor(props) {
        super(props);

        this.onRemove = () => {
            const { id, index } = this.props;
            clippings.remove(id, index);
            images.update(
                this.props.id,
                clippings.points(this.props.id).toJS(),
                () => {}
            );
        };
        this.onHighlight = () => {
            const { id, index } = this.props;
            clippings.highlight(id, index);
        };
    }
    render() {
        const clip = this.props.clip;

        return (
            <div className="Clipping">
                <Button onClick={this.onHighlight}>
                    {pointsToJSON(clip.points)}
                </Button>
                {clip.closed ? (
                    <Button onClick={this.onRemove}>
                        <FormattedMessage id="image_editor.clippings.delete" />
                    </Button>
                ) : null}
            </div>
        );
    }
}

Clipping.propTypes = {
    id: PropTypes.string.isRequired,
    index: PropTypes.number.isRequired,
    clip: PropTypes.object.isRequired
};

class Clippings extends React.Component {
    render() {
        const props = this.props;

        return (
            <div className="Clippings">
                {Object.values(props.list).map((clip, index) => (
                    <Clipping
                        key={index}
                        id={props.id}
                        index={index}
                        clip={clip}
                    />
                ))}
            </div>
        );
    }
}

Clippings.propTypes = {
    id: PropTypes.string.isRequired
};

const pointsToJSON = points => {
    return points.map(point =>
        JSON.stringify([point[0] | 0, point[1] | 0], null, 2)
    );
};

const mapping = (nextState, props) => {
    const clipping = nextState.get("clippings").get(props.id);

    if (clipping) {
        return { list: clipping.get("list").toJS() };
    } else {
        return { list: [] };
    }
};

const shouldUpdate = (prevState, nextState, props) => {
    const id = props.id,
        prev = prevState.get("clippings").get(id),
        next = nextState.get("clippings").get(id);

    if (prev && next) {
        return !prev.equals(next);
    } else if (!prev && next) {
        return true;
    } else {
        return false;
    }
};

export default connect(Clippings, [clippings], mapping, shouldUpdate);
