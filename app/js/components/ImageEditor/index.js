import React from "react";
import { findDOMNode } from "react-dom";
import PropTypes from "prop-types";
import { FormattedMessage } from "react-intl";
import Button from "material-ui/Button";
import TextField from "material-ui/TextField";
import { Screen } from "@nathanfaucett/web_app";
import connect from "@nathanfaucett/state-react";
import ClippingEditor from "../ClippingEditor";
import Clippings from "./Clippings";
import images from "../../stores/images";
import clippings from "../../stores/clippings";
import app from "../../app";

const screenStore = app.plugin(Screen).store();
const imageEditorForm = app.createStore("imageEditorForm", {
    name: "",
    points: []
});

class ImageEditor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentDidMount() {
        images.get(this.props.id, (error, image) => {
            if (!error) {
                clippings.fromPoints(this.props.id, image.get("points").toJS());
            }
        });
        this.onResize();
    }
    componentWillReceiveProps(nextProps) {
        if (
            this.props.screen.width !== nextProps.screen.width ||
            this.props.screen.height !== nextProps.screen.height
        ) {
            this.onResize();
        }
    }
    onResize() {
        const content = findDOMNode(this.refs.content);

        this.setState({
            width: content.offsetWidth,
            height: this.props.screen.height - 72
        });
    }
    render() {
        const props = this.props,
            state = this.state,
            image = props.images[props.id];

        return (
            <div>
                <div className="grid">
                    <div className="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                        <div>
                            <Clippings id={props.id} />
                        </div>
                    </div>
                    <div
                        ref="content"
                        className="col-xs-12 col-sm-8 col-md-8 col-lg-9"
                    >
                        {image ? (
                            <ClippingEditor
                                id={image.id}
                                src={app.config.apiUrl + image.url}
                                width={state.width || 0}
                                height={state.height || 0}
                            />
                        ) : null}
                    </div>
                </div>
            </div>
        );
    }
}

ImageEditor.propTypes = {
    id: PropTypes.string.isRequired
};

export default connect(ImageEditor, [images, screenStore, imageEditorForm]);
