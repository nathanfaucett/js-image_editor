import React from "react";
import Button from "material-ui/Button";
import { withStyles } from "material-ui/styles";
import { FormattedMessage } from "react-intl";
import user from "../../stores/user";
import app from "../../app";

const styles = () => ({
    logo: {
        float: "left"
    },
    profile: {
        float: "right"
    }
});

class Header extends React.Component {
    constructor(props) {
        super(props);

        this.onSignOut = () => {
            user.signOut(() => {
                app.page.go("/sign_in");
            });
        };
    }
    render() {
        return (
            <div className="clear">
                <div className={this.props.classes.logo}>
                    <Button href="/">
                        <FormattedMessage id="nav.home" />
                    </Button>
                </div>
                <div className={this.props.classes.profile}>
                    {user.isSignedIn() ? (
                        <Button onClick={this.onSignOut}>
                            <FormattedMessage id="sign_in.sign_out" />
                        </Button>
                    ) : (
                        <Button href="/sign_in">
                            <FormattedMessage id="sign_in.sign_in" />
                        </Button>
                    )}
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(Header);
