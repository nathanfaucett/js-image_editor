import React from "react";
import Button from "material-ui/Button";
import { Locales } from "@nathanfaucett/web_app";
import { FormattedMessage } from "react-intl";
import { withStyles } from "material-ui/styles";
import app from "../../app";

const styles = () => ({
    container: {
        width: "100%",
        position: "fixed",
        bottom: 0,
        left: 0
    },
    left: {
        float: "left"
    },
    right: {
        float: "right"
    }
});

class Footer extends React.Component {
    createOnClick(locale) {
        return () => {
            app
                .plugin(Locales)
                .store()
                .updateState(state => state.set("locale", locale));
        };
    }
    render() {
        return (
            <div className={this.props.classes.container}>
                <div className="clear">
                    <div className={this.props.classes.left}>
                        <FormattedMessage id="app.name" />
                    </div>
                    <div className={this.props.classes.right}>
                        <Button onClick={this.createOnClick("en")}>
                            <FormattedMessage id="app.locales.en" />
                        </Button>
                        <Button onClick={this.createOnClick("de")}>
                            <FormattedMessage id="app.locales.de" />
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}

export default withStyles(styles)(Footer);
