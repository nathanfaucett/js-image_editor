import React from "react";
import { findDOMNode } from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import app from "../app";

const styles = () => ({
    container: {
        display: "inline-block"
    }
});

class GalleryItem extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            width: this.props.height,
            height: this.props.height,
            aspect: 1
        };

        this.onLoad = () => {
            const node = findDOMNode(this.refs.image),
                width = node.width,
                height = node.height;

            this.setState({
                loaded: true,
                width: width,
                height: height,
                aspect: width / height
            });
        };
    }
    componentDidMount() {
        this._isMounted = true;

        findDOMNode(this.refs.image).onload = () => {
            if (this._isMounted) {
                this.onLoad();
            }
        };
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    render() {
        const styles = {};

        if (!this.state.loaded) {
            styles.display = "none";
        } else {
            styles.width = this.props.height * this.state.aspect + "px";
            styles.height = this.props.height + "px";
        }

        return (
            <div className={this.props.classes.container}>
                <a href={"/editor/" + this.props.image.id}>
                    <img
                        ref="image"
                        style={styles}
                        src={app.config.apiUrl + this.props.image.url}
                    />
                </a>
            </div>
        );
    }
}

GalleryItem.defaultProps = {
    height: 256
};

GalleryItem.propTypes = {
    height: PropTypes.number,
    image: PropTypes.object.isRequired
};

export default withStyles(styles)(GalleryItem);
