import React from "react";
import { findDOMNode } from "react-dom";
import PropTypes from "prop-types";
import connect from "@nathanfaucett/state-react";
import { withStyles } from "material-ui/styles";
import clippings from "../../stores/clippings";

const styles = () => ({
    container: {
        position: "absolute",
        top: 0,
        left: 0
    }
});

class Canvas extends React.Component {
    componentDidMount() {
        const canvas = findDOMNode(this.refs.canvas);
        this.ctx = canvas.getContext("2d");
        this.draw();
    }
    componentDidUpdate() {
        this.draw();
    }
    clear() {
        const { width, height } = this.props;
        this.ctx.clearRect(0, 0, width, height);
    }
    drawPoints() {
        const { current, highlighted, list, fromCoord } = this.props;

        if (list) {
            const ctx = this.ctx;

            ctx.save();
            list.forEach((clip, index) => {
                const points = clip.points,
                    firstPoint = fromCoord(points[0]);

                ctx.beginPath();
                ctx.moveTo(firstPoint[0], firstPoint[1]);

                for (let i = 1, il = points.length; i < il; i++) {
                    let point = fromCoord(points[i]);
                    ctx.lineTo(point[0], point[1]);
                }

                if (clip.closed) {
                    ctx.lineTo(firstPoint[0], firstPoint[1]);
                }

                if (index === highlighted || index === current) {
                    ctx.lineWidth = 4;
                } else {
                    ctx.lineWidth = 2;
                }

                ctx.strokeStyle = clip.color;
                ctx.stroke();
            });
            ctx.restore();
        }
    }
    draw() {
        this.clear();
        this.drawPoints();
    }
    render() {
        const props = this.props;

        return (
            <canvas
                className={props.classes.container}
                ref="canvas"
                width={props.width}
                height={props.height}
            />
        );
    }
}

Canvas.propTypes = {
    id: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    fromCoord: PropTypes.func.isRequired
};

const mapping = (nextState, props) => {
    const clipping = nextState.get("clippings").get(props.id);

    if (clipping) {
        return {
            current: clipping.get("current"),
            highlighted: clipping.get("highlighted"),
            list: clipping.get("list").toJS()
        };
    } else {
        return { current: 0, highlighted: 0, list: [] };
    }
};

const shouldUpdate = (prevState, nextState, props) => {
    const id = props.id,
        prev = prevState.get("clippings").get(id),
        next = nextState.get("clippings").get(id);

    if (prev && next) {
        return !prev.equals(next);
    } else if (!prev && next) {
        return true;
    } else {
        return false;
    }
};

export default connect(
    withStyles(styles)(Canvas),
    [clippings],
    mapping,
    shouldUpdate
);
