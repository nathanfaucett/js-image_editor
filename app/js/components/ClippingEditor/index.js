import React from "react";
import { findDOMNode } from "react-dom";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import clippings from "../../stores/clippings";
import images from "../../stores/images";
import app from "../../app";
import Canvas from "./Canvas";
import Image from "./Image";

const styles = () => ({
    container: {
        position: "relative",
        overflow: "hidden"
    },
    canvas: {
        position: "absolute",
        top: 0,
        left: 0
    }
});

const NONE = 0,
    CLIPPING = 1,
    MIN_DIST = 32;

const mousePosition = (e, node) => {
    const rect = node.getBoundingClientRect(),
        x = e.clientX - rect.left,
        y = e.clientY - rect.top;

    return [x, y];
};

class ClippingEditor extends React.Component {
    constructor(props) {
        super(props);

        this.currentState = NONE;

        this.onMouseMove = e => {
            var image = this.refs.image;

            if (!image || !image.isLoaded()) {
                return;
            }
            this._onMouseMove(
                this.toCoord(mousePosition(e, findDOMNode(image)))
            );
        };
        this.onMouseClick = e => {
            var image = this.refs.image;

            if (!image || !image.isLoaded() || e.which !== 1) {
                return;
            }
            this._onMouseClick(
                this.toCoord(mousePosition(e, findDOMNode(image)))
            );
        };
        this.toCoord = point => {
            const scale = this.refs.image.scale;

            point[0] = point[0] / scale;
            point[1] = point[1] / scale;

            return point;
        };
        this.fromCoord = point => {
            const { scale, width, height } = this.refs.image,
                props = this.props,
                offsetX = (props.width - width) / 2,
                offsetY = (props.height - height) / 2;

            point[0] = point[0] * scale;
            point[1] = point[1] * scale;
            point[0] = offsetX + point[0];
            point[1] = offsetY + point[1];

            return point;
        };
    }
    componentDidMount() {
        const canvas = findDOMNode(this.refs.canvas);
        app.addEventListener(canvas, "mousemove", this.onMouseMove);
        app.addEventListener(canvas, "click", this.onMouseClick);
    }
    componentWillUnmount() {
        const canvas = findDOMNode(this.refs.canvas);
        app.removeEventListener(canvas, "mousemove", this.onMouseMove);
        app.removeEventListener(canvas, "click", this.onMouseClick);
    }
    _onMouseMove(mouse) {
        switch (this.currentState) {
            case NONE:
                clippings.updateInitialPoint(this.props.id, mouse);
                break;
            case CLIPPING:
                clippings.updatePoint(
                    this.props.id,
                    mouse,
                    MIN_DIST / this.refs.image.scale
                );
                break;
        }
    }
    _onMouseClick(mouse) {
        switch (this.currentState) {
            case NONE:
                this.currentState = CLIPPING;
                clippings.addPoint(this.props.id, mouse, NaN);
                break;
            case CLIPPING:
                if (
                    clippings.addPoint(
                        this.props.id,
                        mouse,
                        MIN_DIST / this.refs.image.scale
                    )
                ) {
                    images.update(
                        this.props.id,
                        clippings.points(this.props.id).toJS(),
                        () => {}
                    );
                    this.currentState = NONE;
                }
                break;
        }
    }
    render() {
        const props = this.props,
            containerStyle = {
                width: props.width,
                height: props.height
            };

        return (
            <div className={props.classes.container} style={containerStyle}>
                <Image
                    ref="image"
                    src={props.src}
                    width={props.width}
                    height={props.height}
                />
                <Canvas
                    ref="canvas"
                    id={props.id}
                    width={props.width}
                    height={props.height}
                    fromCoord={this.fromCoord}
                />
            </div>
        );
    }
}

ClippingEditor.propTypes = {
    id: PropTypes.string.isRequired,
    src: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
};

export default withStyles(styles)(ClippingEditor);
