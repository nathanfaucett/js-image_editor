import React from "react";
import { findDOMNode } from "react-dom";
import PropTypes from "prop-types";

class Image extends React.Component {
    constructor(props) {
        super(props);

        this._isMounted = false;
        this.state = {
            loaded: false
        };
    }
    isLoaded() {
        return this.state.loaded;
    }
    componentDidMount() {
        const image = findDOMNode(this.refs.image);

        this._isMounted = true;

        image.onload = () => {
            this.image = image;
            this.imageWidth = image.width;
            this.imageHeight = image.height;

            if (this._isMounted) {
                this.onResize();
            }
        };
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    componentWillReceiveProps(nextProps) {
        const props = this.props;

        if (
            (props.width !== nextProps.width ||
                props.height !== nextProps.height) &&
            this.state.loaded
        ) {
            this.onResize();
        }
    }
    onResize() {
        const { width, height } = this.props,
            aspect = width / height,
            imageWidth = this.imageWidth,
            imageHeight = this.imageHeight,
            imageAspect = imageWidth / imageHeight,
            isVertical = aspect > imageAspect;

        this.aspect = imageAspect;
        this.width = isVertical ? height * imageAspect : width;
        this.height = isVertical ? height : width / imageAspect;
        this.scale = isVertical ? height / imageHeight : width / imageWidth;

        this.setState({
            loaded: true
        });
    }
    render() {
        const style = {
            position: "absolute",
            margin: "auto",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0
        };

        if (this.state.loaded) {
            style.width = this.width;
            style.height = this.height;
            return <img ref="image" style={style} src={this.props.src} />;
        } else {
            style.display = "none";
            return <img ref="image" style={style} src={this.props.src} />;
        }
    }
}

Image.propTypes = {
    src: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired
};

export default Image;
