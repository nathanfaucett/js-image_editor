import app from "../app";
import user from "../stores/user";

const oauthSignIn = (ctx, next) => {
	if (ctx.query && ctx.query.token) {
		user.signInWithTokenId(ctx.query.token, error => {
			if (error) {
				console.error(error);
				app.page.go("/sign_in");
			} else {
				app.page.go("/");
			}
		});
	}
};

export default oauthSignIn;
