import app from "../app";
import user from "../stores/user";

const isSignedIn = (ctx, next) => {
	if (user.isSignedIn()) {
		next();
	} else {
		app.page.go("/sign_in");
	}
};

export default isSignedIn;
