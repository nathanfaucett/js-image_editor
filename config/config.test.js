module.exports = options => ({
	appUrl: "http://test.imageeditor.com:" + options.port,
	apiUrl: "http://api.test.imageeditor.com:" + options.apiPort
});
