module.exports = options => ({
	appUrl: "http://dev.imageeditor.com:" + options.port,
	apiUrl: "http://api.dev.imageeditor.com:" + options.apiPort
});
