const gulp = require("gulp"),
    ParcelBundler = require("parcel-bundler"),
    express = require("express"),
    del = require("del"),
    gulpUtil = require("gulp-util"),
    configBundler = require("@nathanfaucett/config-bundler"),
    localesBundler = require("@nathanfaucett/locales-bundler");

const IS_PROD = process.env.NODE_ENV === "production",
    IS_DEV = !IS_PROD,
    PORT = process.env.PORT || 8080,
    API_PORT = process.env.API_PORT || 4000;

const cleanCache = () => del(["./.cache"]);

gulp.task("clean-cache", cleanCache);

const clean = () => del(["./app/js/config.json", "./build"]);

gulp.task("clean", clean);

const config = () => {
    return gulp
        .src(["./config/*.js"])
        .pipe(
            configBundler({
                options: {
                    port: PORT,
                    apiPort: API_PORT
                }
            })
        )
        .pipe(gulp.dest("./app/js"));
};

gulp.task("config", config);

const locales = () => {
    return gulp
        .src("./config/locales/**/*.json")
        .pipe(
            localesBundler({
                flatten: true,
                minify: IS_PROD
            })
        )
        .pipe(gulp.dest("./build/locales"));
};

gulp.task("locales", locales);

const parcel = () => {
    const bundler = new ParcelBundler("./app/index.html", {
        outDir: "./build",
        outFile: "./build/index.html",
        publicUrl: "./",

        cache: IS_DEV,
        cacheDir: ".cache",

        watch: IS_DEV,
        target: "browser",
        https: false,

        minify: IS_PROD,
        sourceMaps: true,

        hmr: IS_DEV,
        hmrPort: 0,
        hmrHostname: "",

        logLevel: 3,
        detailedReport: false
    });

    if (IS_DEV) {
        const app = express();
        app.use(express.static("build"));
        app.listen(PORT);
        gulpUtil.log("Listening on port " + PORT);
    }

    return bundler.bundle();
};

gulp.task("parcel", parcel);

const watch = () => {
    gulp.watch(["./config/config.*.js", "./config/config.js"], config);
    gulp.watch("./config/locales/**/*.json", locales);
};

gulp.task("watch", watch);

const build = IS_PROD
    ? gulp.series(cleanCache, clean, gulp.parallel(config, locales), parcel)
    : gulp.series(
          clean,
          gulp.parallel(config, locales),
          gulp.parallel(watch, parcel)
      );

gulp.task("build", build);
gulp.task("default", build);
